"use strict";

document.addEventListener('DOMContentLoaded', function () {
  // Custom JS
  var body = document.body;
  var cx = window.innerWidth / 2,
      cy = window.innerHeight / 2,
      clientX = 0,
      clientY = 0;
  body.addEventListener('mousemove', function (e) {
    clientX = e.pageX;
    clientY = e.pageY;
    var request = requestAnimationFrame(updateMe);
  });

  function updateMe() {
    var dx = clientX - cx,
        dy = clientY - cy,
        tiltX = dy / cy,
        tiltY = dx / cx,
        radius = Math.sqrt(Math.pow(tiltX, 2) + Math.pow(tiltY, 2)),
        degree = radius * 12;
    gsap.to('.content', 1, {
      transform: "rotate3d( ".concat(tiltX, ", ").concat(tiltY, ", 0, ").concat(degree, "deg )")
    });
  }
});