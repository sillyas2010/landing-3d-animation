document.addEventListener('DOMContentLoaded', () => {

	// Custom JS
  const body = document.body;
  let cx = window.innerWidth / 2,
      cy = window.innerHeight / 2,
      clientX = 0,
      clientY = 0;

  body.addEventListener('mousemove', e => {
    clientX = e.pageX;
    clientY = e.pageY;

    let request = requestAnimationFrame(updateMe);
  });

  function updateMe() {
    let dx = clientX - cx,
        dy = clientY - cy,
        tiltX = dy / cy,
        tiltY = dx/ cx,
        radius = Math.sqrt(Math.pow(tiltX, 2) + Math.pow(tiltY, 2)),
        degree = radius * 12;

    gsap.to('.content', 1, { transform: `rotate3d( ${tiltX}, ${tiltY}, 0, ${degree}deg )` });
  }
})
